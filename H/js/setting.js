function main() {
    var d = [];
    d.push({
        title: "<b><small>首页选择</small></b>",
        url: "hiker://empty",
        col_type: "avatar",
        img: config.icon + "more/6.png",
    });
    for (var i in config.listname) {
        d.push({
            title: "<b>" + config.listname[i] + "</b>",
            url: $("#noLoading#").lazyRule((j) => {
                setItem("H", j);
                refreshPage(true);
                return "hiker://empty";
            }, i),
            img: getItem("H") == i ? config.icon + "messy/1.svg" : config.icon + "messy/2.svg",
            col_type: "text_icon",
        })

    }
    // if (getItem(arrary) == "on") {
    //     require(config.依赖)
    //     GX();
    // }

    setResult(d);
}
