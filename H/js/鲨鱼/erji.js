function main() {
    let url = MY_PARAMS.url;
    let title = MY_PARAMS.title;
    let description = MY_PARAMS.desc
    let pic = MY_PARAMS.pic_url;
    let a = [];
    a.push({
        title: "‘‘’’<b><small><font color=\"#b0e0e6\">片名:" + "\n" + "\n" + title + "</font></small></b>",
        desc: "‘‘’’<b><small><b><font color=\"#708090\">" + title + "</font></b><font color=\"#778899\">" + description + "</font></small></b>",
        url: pic,
        pic_url: pic,
        col_type: "movie_1_vertical_pic_blur"
    })

    a.push({
        title: "剧情",
        desc: "",
        url: $("hiker://empty#noRecordHistory##noHistory#").rule((description) => {
            setResult([{title: description, col_type: "long_text"}]);
        }, description),
        pic_url: config.icon + "more/8.png",
        col_type: "text_1",
        extra: {"inheritTitle": false}//不继承标题
    })

    var ds = '<span style="color:#19B89D">选集排序<small><font color=\'grey\'>	共' + url.length + '条</font></small></span>'
    a.push({
        title: getItem("zf", "f") == "z" ? ds + '<span style="color: #33cccc">▴</span>' : ds + '<span style="color: #ff7f50">▾</span>',
        col_type: "text_icon",
        url: $("#noLoading#").lazyRule(() => {
            if (getItem("zf", "f") == "z") {
                clearItem("zf");
            } else {
                setItem("zf", "z");
            }
            refreshPage(false);
            return "#noHistory#hiker://empty"
        }),
        pic_url: getItem("zf", "f") == "z" ? config.icon + "messy/3.svg" : config.icon + "messy/4.svg",
    })

    if (getItem("zf", "f") == "z") {

        a.push({
            title: "第1集",
            url: "video://" + url,
            col_type: 'text_4'
        })

    } else {

        a.push({
            title: "第1集",
            url: "video://" + url,
            col_type: 'text_4',
        })

    }
    a.push({
        col_type: 'line'
    })
    a.push({
        desc: "‘‘’’<small><font color=#f20c00>此规则仅限学习交流使用，请于导入后24小时内删除，任何团体或个人不得以任何方式方法传播此规则的整体或部分！</font></small>",
        url: "toast://温馨提示：且用且珍惜！",
        col_type: 'text_center_1',
    })
    a.push({
        col_type: 'line'
    })
    a.push({
        col_type: "big_blank_block"
    })

    a.push({
        col_type: "big_blank_block"
    })
    setResult(a);
}
