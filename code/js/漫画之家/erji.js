function main() {
    let url = MY_PARAMS.url;
    var html = JSON.parse(fetch(url)).data;
    let info = html.info
    let list = html.list.reverse();
    var d = []
    let xiazai = []
    for (var i in list) {
        xiazai.push({
            title: list[i].chapter_name,
            url: "http://api.dmzj.com/dynamic/comicread/" + list[i].comic_id + "/" + list[i].id + ".json"
        })
    }

    d.push({
        title: "‘‘’’<b><small><font color=\"#b0e0e6\">片名:\t" + info.title + "\n作者:\t" + info.authors + "\n类型:\t" + info.types + "\n地区:\t" + info.zone + "\n状态:\t" + info.status + "</font></small></b>",
        desc: "‘‘’’<b><small><font color=\"#778899\">" + info.description + "</font></small></b>",
        url: html.info.cover,
        pic_url: html.info.cover,
        col_type: "movie_1_vertical_pic_blur"
    })
    d.push({
        title: "剧情",
        desc: "",
        url: $("hiker://empty#noRecordHistory##noHistory#").rule((description) => {
            setResult([{title: description, col_type: "long_text"}]);
        }, info.description),
        pic_url: "hiker://files/bgHouse/src/more/8.png",
        col_type: "icon_2",
        extra: {"inheritTitle": false}//不继承标题
    })
    d.push({
        title: "下载",
        desc: "",
        url: "hiker://page/download.view#noRecordHistory##noRefresh##noHistory#?rule=本地资源管理",
        pic_url: "hiker://files/bgHouse/src/system/2.svg",
        col_type: "icon_2",
        extra: {
            chapterList: xiazai,
            info: {
                bookName: info.title,
                bookTopPic: info.cover,
                parseCode: $.toString(() => {
                    function jiexi() {
                        let html = JSON.parse(fetch(url));
                        let pages = html.page
                        let ll = "pics://"
                        for (let i in pages) {
                            ll += pages[i] + '&&';
                        }
                        return ll;


                    };
                    return jiexi(input);
                }),
                ruleName: "BGcode2"
            }
        }
    })

    var ds = '<span style="color:#19B89D">选集排序<small><font color=\'grey\'>	共' + list.length + '条</font></small></span>'
    d.push({
        title: getItem("zf", "f") == "z" ? ds + '<span style="color: #33cccc">▴</span>' : ds + '<span style="color: #ff7f50">▾</span>',
        col_type: "text_icon",
        url: $("#noLoading#").lazyRule(() => {
            if (getItem("zf", "f") == "z") {
                clearItem("zf");
            } else {
                setItem("zf", "z");
            }
            refreshPage(false);
            return "#noHistory#hiker://empty"
        }),
        pic_url: getItem("zf", "f") == "z" ? "hiker://files/bgHouse/src/messy/3.svg" : "hiker://files/bgHouse/src/messy/4.svg",
    })
    if (getItem("zf", "f") == "z") {
        for (var i = 0; i < list.length; i++) {
            d.push({
                title: "第" + (i + 1) + "话",
                url: $().lazyRule((url) => {
                    require("hiker://files/bgHouse/js/漫画之家/erji.js");
                    return jiexi(url)
                }, list[i]), col_type: 'text_4',
            })
        }
    } else {

        for (var i = list.length - 1; i > -1; i--) {
            d.push({
                title: "第" + (i + 1) + "话",
                url: $().lazyRule((url) => {
                    require("hiker://files/bgHouse/js/漫画之家/erji.js");
                    return jiexi(url)
                }, list[i]), col_type: 'text_4',
            })
        }
    }

    setResult(d);
}

function jiexi(x) {
    let url = "http://api.dmzj.com/dynamic/comicread/" + x.comic_id + "/" + x.id + ".json"
    let html = JSON.parse(fetch(url));
    let pages = html.page
    let ll = "pics://"
    for (let i in pages) {
        ll += pages[i] + '&&';
    }
    return ll;
}