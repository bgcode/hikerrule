function main() {
    var d = [];
    let code = fetch(MY_PARAMS.url)
    let pic = xpath(code, "//*[@id=\"Cover\"]/img/@src");
    let tit = xpath(code, "//*[@id=\"comicName\"]/text()");
    let tit1 = xpath(code, "//body/div[1]/div[1]/div[3]/div[2]/p[1]/text()");
    let tit2 = xpath(code, "//body/div[1]/div[1]/div[3]/div[2]/p[2]/a/text()");
     let dec = xpath(code, "//*[@id=\"full-des\"]/text()");
    d.push({
        title: "‘‘’’<b><small><font color=\"#b0e0e6\">片名:\t" + tit + "\n作者:\t" + tit1+ "\n类型:\t" + tit2 + "</font></small></b>",
        desc: "‘‘’’<b><small><font color=\"#778899\">" + dec + "</font></small></b>",
        url: pic,
        pic_url: pic,
        col_type: "movie_1_vertical_pic_blur"
    })
    d.push({
        title: "剧情",
        desc: "",
        url: $("hiker://empty#noRecordHistory##noHistory#").rule((description) => {
            setResult([{title: description, col_type: "long_text"}]);
        }, dec),
        pic_url: "hiker://files/bgHouse/src/more/8.png",
        col_type: "icon_2",
        extra: {"inheritTitle": false}//不继承标题
    })
    d.push({
        title: "下载",
        desc: "",
        url: "hiker://page/download.view#noRecordHistory##noRefresh##noHistory#?rule=本地资源管理",
        pic_url: "hiker://files/bgHouse/src/system/2.svg",
        col_type: "icon_2",
        extra: {
            chapterList: "",
            info: {
                bookName: tit,
                bookTopPic: pic,
                parseCode: $.toString(() => {
                    function jiexi() {
                        let html = JSON.parse(fetch(url));
                        let pages = html.page
                        let ll = "pics://"
                        for (let i in pages) {
                            ll += pages[i] + '&&';
                        }
                        return ll;


                    };
                    return jiexi(input);
                }),
                ruleName: "BGcode2"
            }
        }
    })
    setResult(d);
}